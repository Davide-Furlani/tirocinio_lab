﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderTrigger : MonoBehaviour
{
    public event EventHandler OnPlayerEnterTrigger;

    private void OnTriggerEnter(Collider collider) 
    {
        PlayerLogic player = collider.GetComponent<PlayerLogic>();

        if (player != null) 
        {
            Debug.Log("nella sfera");
            OnPlayerEnterTrigger?.Invoke(this, EventArgs.Empty);
        }
    }
}
