﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSystem : MonoBehaviour {
    private enum State {
        Idle,
        Active,
    }

    [SerializeField] private ColliderTrigger colliderTrigger;
    [SerializeField] private Wave[] waveArray;

    private State state;
    private void Awake() {
        state = State.Idle;
    }

    private void Start() {
        colliderTrigger.OnPlayerEnterTrigger += ColliderTrigger_OnPlayerEnterTrigger;
    }

    private void ColliderTrigger_OnPlayerEnterTrigger(object sender, System.EventArgs e) {
        if (state == State.Idle)
            StartBattle();
        colliderTrigger.OnPlayerEnterTrigger -= ColliderTrigger_OnPlayerEnterTrigger;
    }

    private void StartBattle() {
        Debug.Log("start battle");
        //Instantiate(prefab, new Vector3(40, 1, 5), Quaternion.identity);
        state = State.Active;
    }

    int nWave = 0;
    private void Update() {
        switch (state) {
            case State.Active:
                if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0) {
                    if (nWave < waveArray.Length) {
                        waveArray[nWave].Update();
                        nWave += 1;
                    }
                }
                break;
        }
    }

    [System.Serializable]
    private class Wave {
        [SerializeField] private GameObject[] enemySpawnArray;
        [SerializeField] private GameObject center;
        [SerializeField] private float radius;

        public void Update() {
            SpawnEnemies();
        }

        private void SpawnEnemies() {
            foreach (GameObject enemySpawn in enemySpawnArray) {
                Vector3 spawnPoint = GenerateRandomPositionInArea(center, radius);
                Instantiate(enemySpawn, spawnPoint, Quaternion.identity);
            }
        }

        private Vector3 GenerateRandomPositionInArea(GameObject center, float radius) {
            float angle = Random.Range(0, 2 * Mathf.PI);
            float distance = Random.Range(-radius, radius);

            float x = center.transform.position.x + distance * Mathf.Sin(angle);
            float z = center.transform.position.z + distance * Mathf.Cos(angle); ;
            float y = center.transform.position.y;

            return new Vector3(x, y, z);
        }
    }
}
