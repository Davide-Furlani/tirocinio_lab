using System.Collections;
using System.Collections.Generic;
using SFB;
using TMPro;
using UI;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Audio {

    public class SoundManager : MonoBehaviour {

        /// <summary>
        /// <br>Instance of SoundManager give you access to all public methods.</br>
        /// ( <b>There can only be one</b> )
        /// </summary>
        public static SoundManager Instance { get; private set; }

        [Tooltip("Background audio that has to be analyzed")]
        public AudioSource backgroundMusic;

        /// <summary>
        /// All game sounds
        /// </summary>
        [Header("Audio Info")]
        [Tooltip("Array of game sounds")]
        public Sound[] sounds;

        private EscMenu escMenu;

        [Tooltip("Game audio mixer")]
        public AudioMixer audioMixer;

        /// <summary>
        /// Find and play a sound in game 
        /// </summary>
        /// <param name="soundName"></param>

        public bool isSongPlaying = false;

        private Slider progressBar = null;
        private GameObject obj = null;

        public void PlaySound(string soundName) {
            Sound s = System.Array.Find(sounds, sound => sound.soundName == soundName);

            if (s == null) {
                Debug.LogError("File " + soundName + " not found!");
                return;
            }

            s.audioSource.Play();
            isSongPlaying = true;
        }

        public void PauseSound() {
            if (backgroundMusic != null)
                backgroundMusic.Pause();
        }

        public void ResumeSound() {
            if (backgroundMusic != null)
                backgroundMusic.UnPause();
        }

        /// <summary>
        /// Find and stop a sound in game 
        /// </summary>
        /// <param name="soundName"></param>
        public void StopSound() {
            if (backgroundMusic.clip != null)
                backgroundMusic.Stop();
            isSongPlaying = false;
            if (progressBar != null){
                progressBar.GetComponentInChildren<TMP_Text>().text = "";
            }
        }


        /// <summary>
        /// Find and play as background music a sound in game
        /// </summary>
        /// <param name="soundName"></param>
        public void ChangeBackground(AudioClip clip) {

            if (backgroundMusic != null)
                backgroundMusic.Stop();
            isSongPlaying = false;

            backgroundMusic.clip = clip;

            if (SongController.Instance.bgThread != null)
                if (SongController.Instance.bgThread.IsAlive) {
                    SongController.Instance.bgThread.Abort();
                    // Debug.Log("thread bloccato");
                }

            SongController.Instance.analyze = true;
            SongController.Instance.StartPreprocess();
            backgroundMusic.Play();
            isSongPlaying = true;
        }

        private void Awake() {
            #region Singleton
            if (Instance == null)
                Instance = this;
            else {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            #endregion

            AudioMixerGroup[] audioMixerGroup = audioMixer.FindMatchingGroups("Master");

            foreach (Sound sound in sounds) {
                sound.audioSource = gameObject.AddComponent<AudioSource>();
                sound.audioSource.outputAudioMixerGroup = audioMixerGroup[GetMixerIndex(sound.mixerName)];
                sound.audioSource.clip = sound.soundClip;
                sound.audioSource.volume = sound.soundVolume;
                sound.audioSource.pitch = sound.soundPitch;
                sound.audioSource.loop = sound.soundLoop;
            }

            AudioSource audioSource = GetComponentInChildren<AudioSource>();
            // audioSource.Play();
        }

        private int GetMixerIndex(Sound.MixerName mixerName) {
            int index = 0;

            switch (mixerName) {
                case Sound.MixerName.Music:
                    index = 1;
                    break;
                case Sound.MixerName.Effects:
                    index = 2;
                    break;
                default:
                    break;
            }

            return index;
        }

        /*public void SelectMusic() {
            BrowserProperties bp = new BrowserProperties();
            bp.filter = "MP3 (*.mp3) | *.mp3";
            bp.filterIndex = 0;
            bp.title = "SELECT NEW TRACK";

            string songPath = "";

            new FileBrowser().OpenFileBrowser(bp, path => {
                // if (path != null)
                StartCoroutine(ReadSong(path));
                // else
                songPath = path;
            });

            if (songPath == "") {
                backgroundMusic.UnPause();
                Debug.Log("non ho selezionato");
            }
        }*/

        private IEnumerator ReadSong(string path) {

            using (UnityWebRequest uwr = UnityWebRequestMultimedia.GetAudioClip(path, AudioType.MPEG)) {
                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError) {
                    Debug.Log(uwr.error);
                }
                else {
                    AudioClip uwrAudio = DownloadHandlerAudioClip.GetContent(uwr);
                    uwrAudio.name = GetNameFromPath(path);
                    Debug.Log(path);
                    ChangeBackground(uwrAudio);
                }
            }
        }

        private string GetNameFromPath(string path) {
            string name;
            name = path.Substring(path.LastIndexOf("/") + 1);
            name = name.Replace("%20", " ");
            return name;
        }

        private void Start() {
            escMenu = FindObjectOfType<EscMenu>();
        }



        private void Update() {
            if (Input.GetKeyDown(KeyCode.Q)) {
                Cursor.visible = true;
                backgroundMusic.Pause();
                Cursor.visible = true;
                string[] paths = StandaloneFileBrowser.OpenFilePanel("Title", "", "mp3", false);
                Cursor.visible = true;
                if (paths.Length > 0) {
                    StartCoroutine(ReadSong(new System.Uri(paths[0]).AbsoluteUri));
                    Cursor.visible = true;
                }
                escMenu = FindObjectOfType<EscMenu>();
                escMenu.Resume();
            }

            if (Input.GetKeyDown(KeyCode.Period)) {
                StopSound();
            }

            if (obj == null) {
                obj = GameObject.FindGameObjectWithTag("ProgressBar");
            }
            if (obj != null)
                progressBar = obj.GetComponent<Slider>();

            if (progressBar != null) {
                if (backgroundMusic.clip != null && backgroundMusic.isPlaying) {
                    progressBar.GetComponentInChildren<TMP_Text>().text = backgroundMusic.clip.name;
                    progressBar.maxValue = backgroundMusic.clip.length;
                    progressBar.value = backgroundMusic.time;
                }
            }
        }

    }

}
