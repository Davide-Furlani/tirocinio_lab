﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Stat {
    public class GetPlayerAnimationEvents : MonoBehaviour {

        PlayerCombat playerCombat;
        // Start is called before the first frame update
        void Start() {
            playerCombat = GetComponentInParent<PlayerCombat>();
        }

        public void SetSlashTrue() {
            playerCombat.isSlashing = true;
        }
        public void SetSlashFalse() {
            playerCombat.isSlashing = false;
        }
    }
}
