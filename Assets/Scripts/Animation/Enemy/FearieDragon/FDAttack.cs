﻿using UnityEngine;

namespace Animation.Enemy.FaerieDragon {

    public class FDAttack : MonoBehaviour {

        private UnityEngine.AI.NavMeshAgent Navy;

        [SerializeField]
        private GameObject Ball;

        [SerializeField]
        private GameObject SpawnPoint;

        private GameObject ThisBall;

        void Start() {
            Navy = GetComponentInParent<UnityEngine.AI.NavMeshAgent>();
        }

        private void ChargeSphere() {

            Navy.speed = 0f;
            ThisBall = Instantiate(Ball, SpawnPoint.transform.position, Quaternion.identity);
            ThisBall.GetComponent<FD_Projectile>().enabled = false;

        }
        private void Shoot() {
            if (ThisBall != null) {
                ThisBall.GetComponent<FD_Projectile>().enabled = true;
                ThisBall.GetComponent<SphereCollider>().enabled = true;
                Navy.speed = 5f;
            }
        }

    }

}
