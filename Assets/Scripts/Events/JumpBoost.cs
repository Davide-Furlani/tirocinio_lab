﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Movement;

public class JumpBoost : MonoBehaviour
{

    public float JumpValue = 80f;

    void OnTriggerStay (Collider other)
    {
        
    SimpleCharacterController target = PlayerManager.Instance.playerObject.GetComponent <SimpleCharacterController>();
 
       
       if(other.tag == "Player"){
          target.jumpSpeed = JumpValue;    

       }

    }

    void OnTriggerExit (Collider other){

        SimpleCharacterController targett = PlayerManager.Instance.playerObject.GetComponent <SimpleCharacterController>();

         if(other.tag == "Player"){
          targett.jumpSpeed = 30f;    

       }
    }
}
