using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.VFX;

namespace Stat {

    public class CharacterStats : MonoBehaviour {

        [SerializeField]
        private GameObject TextDamage;

        private Color TextColor;

        [SerializeField]
        private float HighText;

        [SerializeField]
        private VisualEffect VFXGraph;

        private Vector3 TextPos;

        [Header("Character Stats")]
        public int maxHealth = 100;
        public int currentHealth { get; private set; }
        public Stat armor;
        [Header("Character Combat Stats")]
        // public float attackSpeed = 1f;
        [HideInInspector]
        public float attackCooldown = 0f;
        public float attackDelay = 1.5f;
        public Stat damage;
        private Material mat;
        public Image healthFill;
        public Gradient healthGradient;
        private Camera cam;
        private Canvas canvas;

        /// <summary>
        /// Set character max health
        /// </summary>
        /// <param name="value"></param>
        public void SetMaxHealth(int value) {
            maxHealth = value;
        }

        /// <summary>
        /// Set character current health
        /// </summary>
        /// <param name="value"></param>
        public void SetCurrentHealth(int value) {
            currentHealth = value;
        }

        /// <summary>
        /// Decrease character halth base by taken damage
        /// </summary>
        /// <param name="value"></param>
        public void TakeDamage(int value) {

            if (this.tag != "Player")
                ShowFloatingText(value);

            value -= armor.GetValue();
            value = Mathf.Clamp(value, 0, int.MaxValue);

            currentHealth -= value;
            if (currentHealth < 0)
                currentHealth = 0;
            if (currentHealth <= 0)
                Die();
            else {
                if (this.tag != "Player") {
                    // mat.SetInt("gotHit_", 1);
                    Debug.Log("colpito");
                    StartCoroutine(ResetToNormalColor());
                }
            }
        }

        private IEnumerator ResetToNormalColor() {
            yield return new WaitForSeconds(0.3f);
            mat.SetInt("gotHit_", 0);
            // Debug.Log("colpito ma tardi");
        }

        public void RegenHealth(int value) {

            currentHealth += value;
            if (currentHealth > maxHealth)
                currentHealth = maxHealth;

            if (currentHealth <= 0)
                Die();
        }

        /// <summary>
        /// Call on enemy die when health <= 0
        /// </summary>
        public virtual void Die() {
            // Debug.Log(transform.name + " died.");

            if (this.tag != "Player") {
                this.GetComponentInChildren<Collider>().enabled = false;
                this.GetComponent<Enemy.EnemyController>().enabled = false;
                this.GetComponent<NavMeshAgent>().enabled = false;
                StartCoroutine(DissolveAndDIE());
            }
        }

        private IEnumerator DissolveAndDIE() {
            float counter = 0f;

            if (VFXGraph != null) {
                mat.SetInt("DeathActivation_", 1);
                VFXGraph.gameObject.SetActive(true);
                VFXGraph.Play();
            }
            while (mat.GetFloat("DissolveAmount_") < 1f) {
                counter = counter + 0.02f;
                mat.SetFloat("DissolveAmount_", counter);

                yield return new WaitForSeconds(0.05f);
            }
            Destroy(this.gameObject);
        }

        private void Awake() {
            currentHealth = maxHealth;
        }

        protected virtual void Start() {

            if (VFXGraph != null) {
                VFXGraph.Stop();
                VFXGraph.gameObject.SetActive(false);
            }

            mat = this.gameObject.GetComponentInChildren<SkinnedMeshRenderer>().material;
            Camera[] cams = FindObjectsOfType<Camera>();
            foreach (Camera camera in cams) {
                if (camera.tag == "MainCamera")
                    cam = camera;
            }
            if (healthFill.GetComponentInParent<Canvas>() != null)
                canvas = healthFill.GetComponentInParent<Canvas>();
        }


        protected virtual void Update() {

            attackCooldown -= Time.deltaTime;

            if (canvas != null) {
                Vector3 v = cam.transform.position - canvas.transform.position;
                v.x = v.z = 0.0f;
                canvas.transform.LookAt(cam.transform.position - v);
                canvas.transform.Rotate(0, 180, 0);

                healthFill.fillAmount = (float)currentHealth / (float)maxHealth;
                healthFill.color = healthGradient.Evaluate(healthFill.fillAmount);
                if (currentHealth <= 0)
                    this.gameObject.GetComponentInChildren<Canvas>().enabled = false;
            }
        }

        private void ShowFloatingText(int dmg) {

            if (dmg < 10) {
                TextColor = new Color(1f, 1f, 1f, 1f);
            }
            else if (dmg < 50) {
                TextColor = new Color(1f, 0.92f, 0.016f, 1f);
            }
            else {
                TextColor = new Color(1f, 0f, 0f, 1f);
            }

            TextPos = transform.position;
            TextPos.y += HighText;

            GameObject go = Instantiate(TextDamage, TextPos, Quaternion.identity);
            go.GetComponent<TextMesh>().text = dmg.ToString();
            go.GetComponent<TextMesh>().color = TextColor;
        }
    }

}
