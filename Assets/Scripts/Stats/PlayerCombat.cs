﻿using System.Collections;
using Enemy;
using UnityEngine;

namespace Stat {

    [RequireComponent(typeof(CharacterStats))]
    public class PlayerCombat : MonoBehaviour {

        /// <summary>
        /// Set true when character are in combat
        /// </summary>
        // public bool InCombat { get; private set; }

        // public event System.Action OnAttack;

        // private const float combatCalldown = 5;

        // private float lastAttackTime;

        // public Transform atkPoint;
        // public float atkRange = 1.45f;
        // public LayerMask enemyLayers;

        private CharacterStats myStats;

        // private EnemyController enemyController;

        // public float baseAtkRange = 1;
        // public new Camera camera;

        [HideInInspector]
        public bool isSlashing = false;

        private void DoDamage(CharacterStats targetStats) {
            // yield return new WaitForSeconds(delay);

            // if (enemyController != null && enemyController.canDoDamage) {
            targetStats.TakeDamage(myStats.damage.GetValue());
            Debug.Log(targetStats.name + " takes damage: " + myStats.damage.GetValue());
            // }
            // else {
            //     targetStats.TakeDamage(myStats.damage.GetValue());
            //     Debug.Log(targetStats.name + " takes damage: " + myStats.damage.GetValue());
            // }

            // if (targetStats.currentHealth <= 0)
            //     InComabt = false;
        }

        // private void OnDrawGizmos() {

        //     if (atkPoint == null)
        //         return;

        //     Gizmos.color = Color.red;
        //     Gizmos.DrawWireSphere(atkPoint.position, atkRange);
        // }

        private void Awake() {
            // baseAtkRange = atkRange;
            myStats = GetComponent<CharacterStats>();

            // if (gameObject.tag == "Enemy")
            //     enemyController = GetComponent<EnemyController>();
        }

        // private void Update() {
        //     myStats.attackCooldown -= Time.deltaTime;

        //     if (Time.time - lastAttackTime > combatCalldown)
        //         InComabt = false;
        // }

        private void OnTriggerEnter(Collider other) {
            if (isSlashing) {
                CharacterStats targetStats = other.GetComponentInParent<CharacterStats>();
                if (targetStats != null)
                    DoDamage(targetStats);
            }
        }


    }

}
