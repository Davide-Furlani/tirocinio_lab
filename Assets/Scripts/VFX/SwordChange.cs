﻿using System.Collections;
using System.Collections.Generic;
using Animation.Player;
using Audio;
using Stat;
using UnityEngine;
using UnityEngine.VFX;

namespace VFX {

    public class SwordChange : MonoBehaviour {

        private MeshRenderer Sword;
        private Material ActiveMat;

        [SerializeField]
        private GameObject lama;

        private Material Scia;
        private Animator animator;
        private PlayerManager player;

        private Vector3 swordBaseScale;

        public float meanTimeWindow = 0.5f;
        private int BUFFERSIZE;
        private int indexCounter = 0;
        private PlayerCombat playerCombat;
        private PlayerLogic playerLogic;
        private float baseDamage;
        public float meanBass;
        public float meanHigh;
        public float ratio_H_on_B;
        public bool valoreMaggiore = false;

        void Start() {

            animator = GetComponentInParent<Animator>();
            Sword = GetComponent<MeshRenderer>();
            ActiveMat = lama.GetComponent<MeshRenderer>().material;
            Scia = gameObject.GetComponentInChildren<ParticleSystemRenderer>().trailMaterial;
            playerLogic = GetComponentInParent<PlayerLogic>();
            playerCombat = GetComponentInParent<PlayerCombat>();
            if (playerCombat == null)
                Debug.LogError("player combat NULL");
            baseDamage = playerLogic.damage.GetValue();
            BUFFERSIZE = (int)(meanTimeWindow * 50);

            swordBaseScale = lama.transform.localScale;
            ToggleOff();
        }
        public void FixedUpdate() {
            if (SoundManager.Instance.isSongPlaying) {

                // if (animator != null) {
                //     if (SongController.Instance.IsPeak && animator.Attacked)
                //         ToggleOn();
                //     if (!animator.StartedAttack)
                //         ToggleOff();
                // }

                // Debug.Log(Scia.GetInt("OnOff_"));

                // representedBassValue[indexCounter] = SongController.Instance.actualBassValue;
                // representedHighValue[indexCounter] = SongController.Instance.actualHighValue;

                // float BassScale = GetMean(representedBassValue);
                // float HighScale = GetMean(representedHighValue);

                float BassScale = SongController.Instance.actualBassValueBuffer;
                float HighScale = SongController.Instance.actualHighValueBuffer;

                meanBass = SongController.Instance.bassSum;
                meanHigh = SongController.Instance.highSum;

                
                if(meanBass >= meanHigh *1.1 && valoreMaggiore){
                    valoreMaggiore = false;
                    BassScale *= 1.5f;
                }

                if(meanHigh >= meanBass *1.1 && !valoreMaggiore){
                    valoreMaggiore = true;
                }

                if (valoreMaggiore) {
                    ToggleOn();
                }else{
                    ToggleOff();
                }


                ratio_H_on_B = meanHigh / meanBass;
                BassScale *= Mathf.Atan(meanBass / meanHigh) * Mathf.Sqrt(meanBass / meanHigh);
                HighScale *= Mathf.Atan(meanHigh / meanBass) * Mathf.Sqrt(meanHigh / meanBass);

                if (float.IsNaN(BassScale))
                    BassScale = 0;
                if (float.IsNaN(HighScale))
                    HighScale = 0;

                // if (BassScale * 10 < swordBaseScale.z)
                // BassScale = swordBaseScale.z / 10;

                // if (SongController.Instance.IsPeak) {

                //modificatore bassi
                if (BassScale > 0) {
                    ToggleOff2();
                }else{
                    ToggleOn2();
                }


                float bladeScale_z = swordBaseScale.z + (BassScale * 2);
                float bladeScale_y = swordBaseScale.y + (BassScale * 2);
                float bladeScale_x = swordBaseScale.x + (BassScale * 2);
                // float rangeScale = bladeScale_z / swordBaseScale.z;

                // Debug.Log(BassScale+"+"+bladeScale_z+"+"+swordBaseScale.z+"+"+bladeScale_y+"+"+swordBaseScale.y+"+"+bladeScale_x+"+"+swordBaseScale.x+"+"+rangeScale);

                lama.transform.localScale = new Vector3(bladeScale_x * 1.5f,
                                                        bladeScale_y,
                                                        bladeScale_z * 1.1f);
                // playerCombat.atkRange = playerCombat.baseAtkRange * rangeScale;

                // }else{
                // lama.transform.localScale = swordBaseScale;
                //     ToggleOff();
                // }

                playerLogic.damage.SetBaseValue(baseDamage + (baseDamage * HighScale));

                HighScale /= 5f;
                if (HighScale > 1)
                    HighScale = 1;
                ActiveMat.SetFloat("ColorValue_", HighScale);
                Scia.SetFloat("ColorValue_", HighScale);



                UpdateIndex();
            }
        }

        void ToggleOn() {
            // Sword.material = ActiveMat;
            Scia.SetInt("OnOff_", 1);
        }

        void ToggleOn2() {
            // Sword.material = ActiveMat;
            animator.SetBool("runchange", false);
        }

        void ToggleOff() {
            //Sword.material = BaseMat;
            Scia.SetInt("OnOff_", 0);
        }

        void ToggleOff2() {
            //Sword.material = BaseMat;
            animator.SetBool("runchange", true);
        }

        void UpdateIndex() {
            indexCounter++;
            if (indexCounter == BUFFERSIZE)
                indexCounter = 0;
        }

        float GetMean(float[] array) {
            float mean = 0;
            for (int i = 0; i < array.Length; i++) {
                mean += array[i];
            }
            return mean / array.Length;
        }
    }
}
