﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateText : MonoBehaviour
{
     void Update() 
     {
         // look at camera...
         transform.LookAt(2*transform.position - Camera.main.transform.position, -Vector3.up);
         // then lock rotation to Y axis only...
         transform.localEulerAngles = new Vector3(0,transform.localEulerAngles.y,0);
     }
}
