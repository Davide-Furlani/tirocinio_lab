﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTextAnimation : MonoBehaviour
{

    [SerializeField]
    private float timer;

    [SerializeField]
    private Vector3 Offset = new Vector3(0f, 2f, 0f); 

    void Start() {
        Destroy(gameObject, timer);

        transform.localPosition += Offset;
    }

}




