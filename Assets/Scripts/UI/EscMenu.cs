﻿using Audio;
using Console;
using Data;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI {

    public class EscMenu : MonoBehaviour {

        private static bool gamePaused = false;

        public static bool GamePaused {
            get => gamePaused;
        }

        public GameObject pauseMenuUI;
        public GameObject onScreenUI;

        private string dataPath;

        GameObject player;
        private bool wasSongPalaying;

        private void Awake() {
            dataPath = Application.persistentDataPath + "/playerData.json";
        }

        // Update is called once per frame
        void Update() {

            if (Input.GetKeyDown(KeyCode.Escape) && PlayerManager.Instance.playerObject.GetComponent<Stat.CharacterStats>().currentHealth <= 0) {
                    Pause();
                    GoToMainMenu();
                    Cursor.visible = true;
            }else if(Input.GetKeyDown(KeyCode.Escape)){
                if (gamePaused)
                    Resume();
                else
                    Pause();
            }
        }

        public void Resume() {
            Time.timeScale = 1f;
            if (wasSongPalaying) {
                SoundManager.Instance.ResumeSound();
            }
            // Cursor.lockState = CursorLockMode.Locked;
            gamePaused = false;
            pauseMenuUI.SetActive(false);
            onScreenUI.SetActive(true);
            Cursor.visible = false;

        }

        public void Pause() {
            Time.timeScale = 0f;
            if (SoundManager.Instance.isSongPlaying) {
                SoundManager.Instance.PauseSound();
                wasSongPalaying = true;
            }
            // Cursor.lockState = CursorLockMode.None;
            gamePaused = true;
            pauseMenuUI.SetActive(true);
            onScreenUI.SetActive(false);
            DebugController.ShowConsole = false;
            Cursor.visible = true;
        }

        public void GoToMainMenu() {
            Time.timeScale = 1f;
            SoundManager.Instance.StopSound();
            SceneManager.LoadScene("MainMenu");
        }


        /// <summary>
        /// Save current player datas
        /// </summary>
        public void Save() {

            player = PlayerManager.Instance.playerObject;

            PlayerLogic playerLogic = player.GetComponent<PlayerLogic>();
            int sceneIndex = SceneManager.GetActiveScene().buildIndex;
            Vector3 playerPosition = player.transform.position;
            Quaternion playerRotation = player.transform.rotation;
            int health = playerLogic.currentHealth;
            // int money = playerLogic.playerWallet.GetMoney();

            PlayerDatas playerDatas = new PlayerDatas(sceneIndex, playerPosition, playerRotation, health, 0);
            Debug.Log(playerDatas);
            Debug.Log("EscMenu :: Saving datas.");
            SavingSystem.PlayerToJSON(playerDatas, dataPath);
            Resume();

        }

    }
}
