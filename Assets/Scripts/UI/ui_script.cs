﻿using System.Collections;
using Audio;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI {
    public class ui_script : MonoBehaviour {
        public Gradient gradient;

        public Image fill;

        public Image border;

        public Slider healthSlider;

        public TMP_Text healthText;

        public TMP_Text fpsText;

        public TMP_Text maxfps;

        public TMP_Text minfps;

        GameObject player;

        PlayerLogic playerLogic;

        public Color colActive;
        public Color colDisabled;
        public bool valoreMaggiore = false;

        public Image DeathBackground;
        public Gradient DeathBackgroundGradient;
        public Image DeathSkull;
        public Gradient DeathSkullGradient;
        public TMP_Text DeathText;
        public Gradient DeathTextGradient;
        public float DeathAnimationTime = 3f;
        private float startDeathAnimation = 0;

        private GameObject[] freqBar = new GameObject[8];

        private bool deathController = false;

        // Start is called before the first frame update
        void Start() {

            GetBars();
            StartCoroutine(getTarget());
            // healthSlider.GetComponentInChildren<Slider>();
            // healthText.GetComponentInChildren<TMP_Text>();

            fill.color = gradient.Evaluate(1f);
            border.color = gradient.Evaluate(1f);


            InvokeRepeating("FpsUpdate", 0f, 0.3f);
        }

        // Update is called once per frame
        void Update() {
            HealthUpdate();
            FpsMaxMin();
            UpdateBars();
            if (deathController)
                StartCoroutine(DeathAnimation());
        }

        private void GetBars() {
            for (int i = 0; i < 8; i++) {
                string name = "Bar (" + i + ")";
                freqBar[i] = GameObject.Find(name);
            }
        }

        private void UpdateBars() {
            for (int i = 0; i < 8; i++) {
                freqBar[i].GetComponent<Image>().fillAmount = SongController.Instance.eightBandsFreqBuffer[i];
            }

            if (SongController.Instance.bassSum >= SongController.Instance.highSum * 1.1 && valoreMaggiore) {
                valoreMaggiore = false;
            }

            if (SongController.Instance.highSum >= SongController.Instance.bassSum * 1.1 && !valoreMaggiore) {
                valoreMaggiore = true;
            }

            if (valoreMaggiore) {
                for (int i = 0; i < 8; i++) {
                    if (i < 4)
                        freqBar[i].GetComponent<Image>().color = colDisabled;
                    if (i >= 4)
                        freqBar[i].GetComponent<Image>().color = colActive;
                }
            }
            else {
                for (int i = 0; i < 8; i++) {
                    if (i < 4)
                        freqBar[i].GetComponent<Image>().color = colActive;
                    if (i >= 4)
                        freqBar[i].GetComponent<Image>().color = colDisabled;
                }
            }
        }

        void HealthUpdate() {
            int health = 0;

            if (playerLogic != null) {
                health = playerLogic.currentHealth;
                if (health <= 0)
                    deathController = true;
            }

            healthSlider.value = health;
            healthText.text = health.ToString();
            fill.color = gradient.Evaluate(healthSlider.normalizedValue);
            border.color = gradient.Evaluate(healthSlider.normalizedValue);
        }

        IEnumerator DeathAnimation() {
            yield return new WaitForSeconds(1.5f);
            startDeathAnimation += Time.deltaTime / DeathAnimationTime;
            if (startDeathAnimation > 1)
                startDeathAnimation = 1;
            DeathBackground.color = DeathBackgroundGradient.Evaluate(startDeathAnimation);
            DeathSkull.color = DeathSkullGradient.Evaluate(startDeathAnimation);
            DeathText.color = DeathTextGradient.Evaluate(startDeathAnimation);
            DeathSkull.GetComponentInChildren<TMP_Text>().color = DeathSkullGradient.Evaluate(startDeathAnimation);

        }

        void FpsUpdate() {
            int fps = (int)(1 / Time.unscaledDeltaTime);
            fpsText.SetText(fps.ToString());
        }

        void FpsMaxMin() {
            int fps = (int)(1 / Time.unscaledDeltaTime);
            if (fps < int.Parse(minfps.text))
                minfps.SetText(fps.ToString());

            if (fps > int.Parse(maxfps.text))
                maxfps.SetText(fps.ToString());
        }

        private IEnumerator InitMinMax() {
            yield return new WaitForSeconds(2f);

            int fps = (int)(1 / Time.unscaledDeltaTime);
            maxfps.SetText(fps.ToString());
            minfps.SetText(fps.ToString());
        }

        private IEnumerator getTarget() {
            yield return new WaitForSeconds(0.5f);

            player = PlayerManager.Instance.playerObject;
            playerLogic = player.GetComponent<PlayerLogic>();

        }

    }
}
