﻿using Interactions;
using Stat;
using UnityEngine;

namespace Enemy.Fly {

    [RequireComponent(typeof(CharacterStats))]
    public class FlyEnemy : EnemyController {
        protected override void AttackPlayer() {
            Attack();
        }

        public override void DoDamage() {
            if (distance <= agent.stoppingDistance) {
                playerStats.TakeDamage(myStats.damage.GetValue());

                Debug.Log(playerStats.name + " takes damage: " + myStats.damage.GetValue());

            }
        }

    }

}
