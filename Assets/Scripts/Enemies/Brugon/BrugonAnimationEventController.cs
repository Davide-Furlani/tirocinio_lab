﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enemy;

public class BrugonAnimationEventController : MonoBehaviour
{
    public void DoDamage(){
        GetComponentInParent<EnemyController>().DoDamage();
    }
}
