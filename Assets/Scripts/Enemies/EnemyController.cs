﻿using System.Collections;
using Stat;
using UnityEngine;
using UnityEngine.AI;

namespace Enemy {

    public class EnemyController : MonoBehaviour {

        /// <summary>
        /// Enemy view distance
        /// </summary>
        [Header("Enemy properties")]
        [Tooltip("Enemy view distance")]
        public float lookRadius = 10;
        [HideInInspector]
        public bool canDoDamage;
        public event System.Action onEnemyMove;
        public event System.Action OnAttack;
        protected CharacterCombat enemyCombat;
        protected Transform target;
        protected NavMeshAgent agent;
        protected CharacterStats myStats;
        protected CharacterStats playerStats;
        protected float distance;

        protected virtual void Start() {
            StartCoroutine(TargetPlayer());
            agent = GetComponent<NavMeshAgent>();
            enemyCombat = GetComponent<CharacterCombat>();
            myStats = GetComponent<CharacterStats>();
        }

        protected virtual void Update() {
            distance = float.MaxValue;

            if (target != null)
                distance = Vector3.Distance(target.position, transform.position);

            if (distance <= lookRadius) {
                agent.SetDestination(target.position);

                if (onEnemyMove != null)
                    onEnemyMove();

                FaceTarget();

                if (distance <= agent.stoppingDistance && myStats.attackCooldown <= 0) {
                    myStats.attackCooldown = myStats.attackDelay;
                    AttackPlayer();
                }
            }
        }


        protected virtual void AttackPlayer() {
            // CharacterStats playerStats = target.GetComponent<CharacterStats>();
            // if (playerStats != null)
            //     Attack(playerStats);

            // PlayerLogic.Killer = gameObject;
        }

        public virtual void DoDamage() { }

        public void Attack() {
            if (OnAttack != null)
                OnAttack();
        }


        protected void FaceTarget() {
            Vector3 direction = (target.position - transform.position).normalized;

            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
        }
        private void OnDrawGizmos() {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, lookRadius);
        }
        private IEnumerator TargetPlayer() {
            yield return new WaitForSeconds(1);

            target = PlayerManager.Instance.playerObject.transform;
            playerStats = PlayerManager.Instance.playerObject.GetComponent<CharacterStats>();
        }
    }
}
