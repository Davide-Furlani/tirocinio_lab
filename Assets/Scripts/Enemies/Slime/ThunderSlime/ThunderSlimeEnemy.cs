﻿using Stat;
using UnityEngine;

namespace Enemy.Slime.ThunderSlime {

    [RequireComponent(typeof(CharacterStats))]
    public class ThunderSlimeEnemy : EnemyController {

        protected override void AttackPlayer() {
            Attack();
        }

        public override void DoDamage() {

            if (distance <= agent.stoppingDistance) {
                playerStats.TakeDamage(myStats.damage.GetValue());

                Debug.Log(playerStats.name + " takes damage: " + myStats.damage.GetValue());

                // if (targetStats.currentHealth <= 0)
                // InCombat = false;
            }
        }
    }
}
