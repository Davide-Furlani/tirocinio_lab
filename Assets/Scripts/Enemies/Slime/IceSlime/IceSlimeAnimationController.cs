﻿
namespace Animation.Enemy.IceSlime {

    public class IceSlimeAnimationController : CharacterAnimator {

        protected override void Start() {
            base.Start();
        }

        protected override void OnEnemyMove() {
            base.OnEnemyMove();
        }

        protected override void OnAttack() {
            base.OnAttack();
        }

    }

}