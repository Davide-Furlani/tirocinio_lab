﻿using UnityEngine;

namespace Animation.Enemy.IceSlime {

    public class IS_Attack : MonoBehaviour {

        private UnityEngine.AI.NavMeshAgent Navy;

        [SerializeField]
        private GameObject Lance;

        [SerializeField]
        private GameObject SpawnPoint;

        private GameObject ThisLance;

        void Start(){
            Navy = GetComponentInParent<UnityEngine.AI.NavMeshAgent>();
        }

        private void ChargeSphere() {

            Navy.speed = 0f;
            ThisLance = Instantiate(Lance, SpawnPoint.transform.position, Quaternion.identity);
            ThisLance.GetComponent<FD_Projectile>().enabled = false;

        }
        private void Shoot() {
            ThisLance.GetComponent<FD_Projectile>().enabled = true;
            Navy.speed = 5f;
        }

    }

}
