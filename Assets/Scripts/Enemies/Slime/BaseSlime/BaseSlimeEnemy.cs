﻿using Stat;
using UnityEngine;

namespace Enemy.BaseSlime {

    [RequireComponent(typeof(CharacterStats))]
    public class BaseSlimeEnemy : EnemyController {
        
        protected override void AttackPlayer() {
            Attack();
        }

        public override void DoDamage() {
            if (distance <= agent.stoppingDistance) {
                // yield return new WaitForSeconds(delay);
                playerStats.TakeDamage(myStats.damage.GetValue());

                Debug.Log(playerStats.name + " takes damage: " + myStats.damage.GetValue());

                // if (targetStats.currentHealth <= 0)
                // InCombat = false;
            }
        }
    }
}
