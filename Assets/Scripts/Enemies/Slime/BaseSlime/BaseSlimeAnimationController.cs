﻿
namespace Animation.Enemy.BaseSlime {

    public class BaseSlimeAnimationController : CharacterAnimator {

        protected override void Start() {
            base.Start();
        }

        protected override void OnEnemyMove() {
            base.OnEnemyMove();
        }

        protected override void OnAttack() {
            base.OnAttack();
        }

    }

}