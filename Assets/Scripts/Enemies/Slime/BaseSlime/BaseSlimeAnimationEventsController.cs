﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Enemy;

public class BaseSlimeAnimationEventsController : MonoBehaviour
{
    public void DoDamage(){
        GetComponentInParent<EnemyController>().DoDamage();
    }
}
