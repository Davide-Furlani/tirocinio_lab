﻿using Interactions;
using Stat;
using UnityEngine;

namespace Enemy.Slime.FireSlime {

    [RequireComponent(typeof(CharacterStats))]
    public class FireSlimeEnemy : EnemyController {
        protected override void AttackPlayer() {
            CharacterStats playerStats = target.GetComponent<CharacterStats>();
            Attack();

            PlayerLogic.Killer = gameObject;
        }

        public override void DoDamage() {
            if (distance <= agent.stoppingDistance) {
                // yield return new WaitForSeconds(delay);
                playerStats.TakeDamage(myStats.damage.GetValue());

                Debug.Log(playerStats.name + " takes damage: " + myStats.damage.GetValue());

                // if (targetStats.currentHealth <= 0)
                // InCombat = false;
            }
        }
    }
}
