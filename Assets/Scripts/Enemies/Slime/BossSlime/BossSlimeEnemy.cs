﻿using Stat;
using UnityEngine;

namespace Enemy.BossSlime {

    [RequireComponent(typeof(CharacterStats))]
    public class BossSlimeEnemy : EnemyController {

        protected override void AttackPlayer() {
            Attack();
        }

        public override void DoDamage() {

            if (distance <= agent.stoppingDistance) {
                playerStats.TakeDamage(myStats.damage.GetValue());

                Debug.Log(playerStats.name + " takes damage: " + myStats.damage.GetValue());

                // if (targetStats.currentHealth <= 0)
                // InCombat = false;
            }
        }
    }
}
