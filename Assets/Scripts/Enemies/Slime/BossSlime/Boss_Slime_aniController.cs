﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using Enemy;

public class Boss_Slime_aniController : MonoBehaviour
{

    private bool SlamController = false;

    [SerializeField]
    private Animator boss;

    [SerializeField]
    private VisualEffect VFXGraph1;
    [SerializeField]
    private VisualEffect VFXGraph2;
    [SerializeField]
    private VisualEffect VFXGraph3;
    [SerializeField]
    private VisualEffect VFXGraph4;
    [SerializeField]
    private VisualEffect VFXGraph5;
    [SerializeField]
    private VisualEffect VFXGraph6;
    

    [SerializeField]
    private GameObject Lightnings1;
    [SerializeField]
    private GameObject Lightnings2;
    [SerializeField]
    private GameObject Lightnings3;
    [SerializeField]
    private GameObject Lightnings4;
    [SerializeField]
    private GameObject Lightnings5;
    [SerializeField]
    private GameObject Lightnings6;

    [SerializeField]
    private GameObject Crack;



    public int counter = 0;

    //private int atkc = 1;

    private Vector4 Fire = new Vector4(4.0f, 1.6f, 0.0f, 1.0f);
    private Vector4 FireParticles = new Vector4(6.3f, 3.9f, 1.5f, 1.0f);
    private Vector4 Ice = new Vector4(1.8f, 4.7f, 4.6f, 1.0f);
    private Vector4 Thunder = new Vector4(0.44f, 0.31f, 0.54f, 1.0f);
    private Vector4 ThunderParticles = new Vector4(4.22f, 3.23f, 0.0f, 1.0f);
    private Gradient FireGradient;
    private GradientColorKey[] FireColorKey;
    private GradientAlphaKey[] FireAlphaKey;
    private Gradient IceGradient;
    private GradientColorKey[] IceColorKey;
    private Gradient ThunderGradient;
    private GradientColorKey[] ThunderColorKey;

    private Color c1 = new Color (3.5f, 0f, 0f, 1f);
    private Color c2 = new Color (2.9f, 0.8f, 0.1f, 1f);
    private Color c3 = new Color (1.8f, 1.2f, 0.3f, 1f);
    private Color c4 = new Color (0.5f, 0.5f, 0.5f, 1f);
    private Color c5 = new Color (0.0f, 1.9f, 3.6f, 1f);
    private Color c6 = new Color (0.1f, 2.75f, 2.95f, 1f);
    private Color c7 = new Color (0.3f, 1.75f, 1.5f, 1f);
    private Color c8 = new Color (3.6f, 2.9f, 0.0f, 1f);
    private Color c9 = new Color (2.94f, 2.6f, 1.3f, 1f);


    // Start is called before the first frame update
    void Awake()
    {   
        /////////////////SETTING GRADIENTS/////////////////////


        //////////FIRE///////////

        FireGradient = new Gradient();
        FireColorKey = new GradientColorKey[4];
        FireAlphaKey = new GradientAlphaKey[3];

        FireColorKey[0].color = c1;
        FireColorKey[0].time = 0.0f;
        FireColorKey[1].color = c2;
        FireColorKey[1].time = 0.25f;
        FireColorKey[2].color = c3;
        FireColorKey[2].time = 0.6f;
        FireColorKey[3].color = c4;
        FireColorKey[3].time = 1.0f;

        FireAlphaKey[0].alpha = 1.0f;
        FireAlphaKey[0].time = 0.0f;
        FireAlphaKey[1].alpha = 0.8f;
        FireAlphaKey[1].time = 0.67f;
        FireAlphaKey[2].alpha = 0.0f;
        FireAlphaKey[2].time = 1.0f;

        FireGradient.SetKeys(FireColorKey, FireAlphaKey);     


        ///////////ICE//////////////

        IceGradient = new Gradient();
        IceColorKey = new GradientColorKey[4];

        IceColorKey[0].color = c5;
        IceColorKey[0].time = 0.0f;
        IceColorKey[1].color = c6;
        IceColorKey[1].time = 0.25f;
        IceColorKey[2].color = c7;
        IceColorKey[2].time = 0.6f;
        IceColorKey[3].color = c4;
        IceColorKey[3].time = 1.0f;  

        IceGradient.SetKeys(IceColorKey, FireAlphaKey);


        /////////THUNDER//////////////

        ThunderGradient = new Gradient();
        ThunderColorKey = new GradientColorKey[4];

        ThunderColorKey[0].color = c8;
        ThunderColorKey[0].time = 0.0f;
        ThunderColorKey[1].color = c9;
        ThunderColorKey[1].time = 0.25f;
        ThunderColorKey[2].color = c7;
        ThunderColorKey[2].time = 0.6f;
        ThunderColorKey[3].color = c4;
        ThunderColorKey[3].time = 1.0f;  

        ThunderGradient.SetKeys(ThunderColorKey, FireAlphaKey);        
        


        //////////////////////////////////////////////////////

    Lightnings1.SetActive(false);
    Lightnings2.SetActive(false);
    Lightnings3.SetActive(false);
    Lightnings4.SetActive(false);
    Lightnings5.SetActive(false);
    Lightnings6.SetActive(false);

        if(VFXGraph1 != null)
        {
            VFXGraph1.Stop();
            VFXGraph1.gameObject.SetActive(false);
            VFXGraph2.Stop();
            VFXGraph2.gameObject.SetActive(false);
            VFXGraph3.Stop();
            VFXGraph3.gameObject.SetActive(false);
            VFXGraph4.Stop();
            VFXGraph4.gameObject.SetActive(false);
            VFXGraph5.Stop();
            VFXGraph5.gameObject.SetActive(false);
            VFXGraph6.Stop();
            VFXGraph6.gameObject.SetActive(false);
            
        } 
      //InvokeRepeating("AttackCount", 2.0f, 5.0f);

    }

    /*void AttackCount(){

        


        switch(atkc)
        {
            case 3:
                boss.SetInteger("attackNumber", 3);
                atkc = 1;
                break;

            case 2:
                boss.SetInteger("attackNumber", 2);
                atkc = 3;
                break;

            case 1:
                boss.SetInteger("attackNumber", 1);
                atkc = 2;
                break;

            default:
                boss.SetInteger("attackNumber", 0);
                break;
        }
    }*/



    private void ElementalAttack(){


        if(counter <= 2){
            counter = counter + 1;
        }else{counter = 0;}


        switch(counter){

            case 3:
                if(boss.GetInteger("attackNumber") != 1)
                {
                    Lightnings1.SetActive(true);
                    VFXGraph1.gameObject.SetActive(true); 
                    VFXGraph1.SetVector4("SmokeColor", Thunder);
                    VFXGraph1.SetVector4("ParticlesColor", ThunderParticles);
                    VFXGraph1.SetGradient("TrailsColor", ThunderGradient);
                    Lightnings2.SetActive(true);
                    VFXGraph2.gameObject.SetActive(true); 
                    VFXGraph2.SetVector4("SmokeColor", Thunder);
                    VFXGraph2.SetVector4("ParticlesColor", ThunderParticles);
                    VFXGraph2.SetGradient("TrailsColor", ThunderGradient);
                    Lightnings3.SetActive(true);
                    VFXGraph3.gameObject.SetActive(true); 
                    VFXGraph3.SetVector4("SmokeColor", Thunder);
                    VFXGraph3.SetVector4("ParticlesColor", ThunderParticles);
                    VFXGraph3.SetGradient("TrailsColor", ThunderGradient);
                }else{
                    Lightnings4.SetActive(true);
                    VFXGraph4.gameObject.SetActive(true); 
                    VFXGraph4.SetVector4("SmokeColor", Thunder);
                    VFXGraph4.SetVector4("ParticlesColor", ThunderParticles);
                    VFXGraph4.SetGradient("TrailsColor", ThunderGradient);
                    Lightnings5.SetActive(true);
                    VFXGraph5.gameObject.SetActive(true); 
                    VFXGraph5.SetVector4("SmokeColor", Thunder);
                    VFXGraph5.SetVector4("ParticlesColor", ThunderParticles);
                    VFXGraph5.SetGradient("TrailsColor", ThunderGradient);
                    Lightnings6.SetActive(true);
                    VFXGraph6.gameObject.SetActive(true); 
                    VFXGraph6.SetVector4("SmokeColor", Thunder);
                    VFXGraph6.SetVector4("ParticlesColor", ThunderParticles);
                    VFXGraph6.SetGradient("TrailsColor", ThunderGradient);
                }
                
                break;
            
            case 2:
                if(boss.GetInteger("attackNumber") != 1)
                {
                    VFXGraph1.gameObject.SetActive(true); 
                    VFXGraph1.SetVector4("SmokeColor", Ice);
                    VFXGraph1.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph1.SetGradient("TrailsColor", IceGradient);
                    VFXGraph2.gameObject.SetActive(true); 
                    VFXGraph2.SetVector4("SmokeColor", Ice);
                    VFXGraph2.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph2.SetGradient("TrailsColor", IceGradient);
                    VFXGraph3.gameObject.SetActive(true); 
                    VFXGraph3.SetVector4("SmokeColor", Ice);
                    VFXGraph3.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph3.SetGradient("TrailsColor", IceGradient);
                }else{
                    VFXGraph4.gameObject.SetActive(true); 
                    VFXGraph4.SetVector4("SmokeColor", Ice);
                    VFXGraph4.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph4.SetGradient("TrailsColor", IceGradient);
                    VFXGraph5.gameObject.SetActive(true); 
                    VFXGraph5.SetVector4("SmokeColor", Ice);
                    VFXGraph5.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph5.SetGradient("TrailsColor", IceGradient);
                    VFXGraph6.gameObject.SetActive(true); 
                    VFXGraph6.SetVector4("SmokeColor", Ice);
                    VFXGraph6.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph6.SetGradient("TrailsColor", IceGradient);
                }

                break;

            case 1:
                if(boss.GetInteger("attackNumber") != 1)
                {
                    VFXGraph1.gameObject.SetActive(true); 
                    VFXGraph1.SetVector4("SmokeColor", Fire);
                    VFXGraph1.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph1.SetGradient("TrailsColor", FireGradient);
                    VFXGraph2.gameObject.SetActive(true); 
                    VFXGraph2.SetVector4("SmokeColor", Fire);
                    VFXGraph2.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph2.SetGradient("TrailsColor", FireGradient);
                    VFXGraph3.gameObject.SetActive(true); 
                    VFXGraph3.SetVector4("SmokeColor", Fire);
                    VFXGraph3.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph3.SetGradient("TrailsColor", FireGradient);
                }else{
                    VFXGraph4.gameObject.SetActive(true); 
                    VFXGraph4.SetVector4("SmokeColor", Fire);
                    VFXGraph4.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph4.SetGradient("TrailsColor", FireGradient);
                    VFXGraph5.gameObject.SetActive(true); 
                    VFXGraph5.SetVector4("SmokeColor", Fire);
                    VFXGraph5.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph5.SetGradient("TrailsColor", FireGradient);
                    VFXGraph6.gameObject.SetActive(true); 
                    VFXGraph6.SetVector4("SmokeColor", Fire);
                    VFXGraph6.SetVector4("ParticlesColor", FireParticles);
                    VFXGraph6.SetGradient("TrailsColor", FireGradient);
                }

                break;

            default:

                VFXGraph1.gameObject.SetActive(false);
                Lightnings1.SetActive(false);
                VFXGraph2.gameObject.SetActive(false);
                Lightnings2.SetActive(false);
                VFXGraph3.gameObject.SetActive(false);
                Lightnings3.SetActive(false);
                VFXGraph4.gameObject.SetActive(false);
                Lightnings4.SetActive(false);
                VFXGraph5.gameObject.SetActive(false);
                Lightnings5.SetActive(false);
                VFXGraph6.gameObject.SetActive(false);
                Lightnings6.SetActive(false);
                break;                
        }

        

    }


    private void ResetElement(){

            VFXGraph1.gameObject.SetActive(false);
            Lightnings1.SetActive(false);
            VFXGraph2.gameObject.SetActive(false);
            Lightnings2.SetActive(false);
            VFXGraph3.gameObject.SetActive(false);
            Lightnings3.SetActive(false);
            VFXGraph4.gameObject.SetActive(false);
            Lightnings4.SetActive(false);
            VFXGraph5.gameObject.SetActive(false);
            Lightnings5.SetActive(false);
            VFXGraph6.gameObject.SetActive(false);
            Lightnings6.SetActive(false);
            
    }

    private void CrackEnter(){

        Instantiate(Crack, transform.position, Quaternion.identity);
    }

    private void ComboAtk(){

        switch(boss.GetInteger("attackNumber"))
        {
            case 3:
                boss.SetInteger("attackNumber", 1);
                break;

            case 2:
                boss.SetInteger("attackNumber", 3);
                break;

            case 1:
                boss.SetInteger("attackNumber", 2);
                break;

            default:
                boss.SetInteger("attackNumber", 1);
                break;
        }
    }

    private void ActivateSlam(){

        SlamController = true;
 
    }

    private void DeactivateSlam(){

        SlamController = false;
    }

    private void OnTriggerStay(Collider other){

        if(other.tag == "Player" && SlamController){
            PlayerLogic target = PlayerManager.Instance.playerObject.GetComponent<PlayerLogic>();
            target.TakeDamage(5);
        }
    }
 



}
