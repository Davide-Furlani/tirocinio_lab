﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightJab : MonoBehaviour
{

    private int element;

    [SerializeField]
    private GameObject Base;

    [SerializeField]
    private GameObject Fire;

    [SerializeField]
    private GameObject Thunder;

    [SerializeField]
    private GameObject Ice;

    [SerializeField]
    private Animator bossanim;

    [SerializeField]
    private GameObject SpawnPoint;

    private void OnTriggerEnter(Collider other) {

        if (other.tag == "Player" && bossanim.GetInteger("attackNumber") == 1){

            PlayerLogic target = PlayerManager.Instance.playerObject.GetComponent<PlayerLogic>();
            target.TakeDamage(10);



            switch(GetComponentInParent<Boss_Slime_aniController>().counter){
                
                case 3:
                    Instantiate(Thunder, SpawnPoint.transform.position, Quaternion.identity);
                break;

                case 2:
                    Instantiate(Ice, SpawnPoint.transform.position, Quaternion.identity);
                break;

                case 1:
                    Instantiate(Fire, SpawnPoint.transform.position, Quaternion.identity);
                break;

                default:
                    Instantiate(Base, SpawnPoint.transform.position, Quaternion.identity);
                break;
            }


        }
    }
}
