﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FD_Projectile : MonoBehaviour
{   
    public float speed;
    private Transform player;
    private Vector3 target;

    

    void Awake(){

        transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform.position, -Vector3.up);
         // then lock rotation to Y axis only...
         transform.localEulerAngles = new Vector3(0,transform.localEulerAngles.y,0);
    }

    // Start is called before the first frame update
    void Start()
    {  
        
        player = GameObject.FindGameObjectWithTag("Player").transform;
        target = new Vector3 (player.position.x, player.position.y, player.position.z);
        transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform.position, -Vector3.up);
         // then lock rotation to Y axis only...
         transform.localEulerAngles = new Vector3(0,transform.localEulerAngles.y,0);

       
        
    }

    // Update is called once per frame
    void Update()
    { 
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
    }




}

